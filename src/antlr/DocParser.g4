parser grammar DocParser;

options {
  tokenVocab=DocLexer;
}

// Parser rules
// ============
program
  : (classDeclaration | namespaceDeclaration)* EOF
  ;

classDeclaration
  : KEYWORD_CLASS name=IDENTIFIER (':' KEYWORD_PUBLIC parent=IDENTIFIER)? '{' KEYWORD_PUBLIC ':' content=memberDeclarationList? '}' ';'
  ;

namespaceDeclaration
  : KEYWORD_NAMESPACE name=IDENTIFIER '{' content=memberDeclarationList? '}' ';'
  ;

memberDeclarationList
  : (variableDeclaration | functionDeclaration | enumDeclaration)+
  ;

variableDeclaration
  : isConst=KEYWORD_CONST? variableType=type name=IDENTIFIER ('=' LITERAL_REAL)? ';'
  ;

functionDeclaration
  : functionType=type name=IDENTIFIER '(' parameters=functionParameterList? ')' ';'
  ;

functionParameterList
  : parameters+=functionParameter (',' parameters+=functionParameter)*
  ;

functionParameter
  : parameterType=type name=IDENTIFIER
  | parameterType=type name=TYPE_TEXT //< Hack for function with a parameter name Text
  ;

enumDeclaration
  : KEYWORD_ENUM name=IDENTIFIER '{' enumValueList '}' ';'
  ;

enumValueList
  : values+=enumValue (',' values+=enumValue)* ','?
  ;

enumValue
  : IDENTIFIER? name=IDENTIFIER
  | KEYWORD_RESERVED
  | KEYWORD_UNUSED
  ;

type
  : typeLiteral
  | typeLiteral isValueConst=KEYWORD_CONST
  | typeClass
  | typeClass isPointer=OPERATOR_POINTER
  | typeClass isValueConst=KEYWORD_CONST
  | typeClass isPointer=OPERATOR_POINTER isValueConst=KEYWORD_CONST
  | typeClass isPointerConst=KEYWORD_CONST isPointer=OPERATOR_POINTER isValueConst=KEYWORD_CONST
  | typeEnum
  | typeEnum isValueConst=KEYWORD_CONST
  | typeArray
  ;

typeArray
  : KEYWORD_ARRAY '<' type '>'
  ;

typeClass
  : IDENTIFIER
  ;

typeEnum
  : IDENTIFIER '::' IDENTIFIER
  ;

typeLiteral
  : TYPE_BOOLEAN
  | TYPE_IDENT
  | TYPE_INT2
  | TYPE_INT3
  | TYPE_INTEGER
  | TYPE_REAL
  | TYPE_TEXT
  | TYPE_VEC2
  | TYPE_VEC3
  | TYPE_VOID
  ;
