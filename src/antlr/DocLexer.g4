lexer grammar DocLexer;

// Comments
// --------
SINGLE_LINE_COMMENT : '//' ~[\r\n\u2028\u2029]* -> channel(HIDDEN) ;
MULTI_LINES_COMMENT : '/*' .*? '*/' -> channel(HIDDEN) ;

// Struct
// ------
STRUCT_IGNORE : 'struct' FRAGMENT_WHITESPACE FRAGMENT_IDENTIFIER FRAGMENT_WHITESPACE? '{' .*? '}' ';' -> channel(HIDDEN) ;
TEMPLATE_IGNORE : 'template <' .*? '>' -> channel(HIDDEN) ;

// Keywords
// --------
KEYWORD_CLASS : 'class' ;
KEYWORD_PUBLIC : 'public' ;
KEYWORD_ENUM : 'enum' ;
KEYWORD_CONST : 'const' ;
KEYWORD_ARRAY : 'Array' ;
KEYWORD_RESERVED : '(reserved)' ;
KEYWORD_UNUSED : '*unused*' ;
KEYWORD_NAMESPACE : 'namespace' ;

// Types
// -----
TYPE_BOOLEAN : 'Boolean' ;
TYPE_IDENT : 'Ident' ;
TYPE_INT2 : 'Int2' ;
TYPE_INT3 : 'Int3' ;
TYPE_INTEGER : 'Integer' ;
TYPE_REAL : 'Real' ;
TYPE_TEXT : 'Text' ;
TYPE_VEC2 : 'Vec2' ;
TYPE_VEC3 : 'Vec3' ;
TYPE_VOID : 'Void' ;

// Operators
// ---------
OPERATOR_COLON : ':' ;
OPERATOR_DOUBLECOLON : '::' ;
OPERATOR_COMMA : ',' ;
OPERATOR_ASSIGN : '=' ;
OPERATOR_SEMICOLON : ';' ;
OPERATOR_OPEN_BRACE : '{' ;
OPERATOR_CLOSE_BRACE : '}' ;
OPERATOR_OPEN_ANGLE : '<' ;
OPERATOR_CLOSE_ANGLE : '>' ;
OPERATOR_OPEN_PAREN : '(' ;
OPERATOR_CLOSE_PAREN : ')' ;
OPERATOR_POINTER : '*' ;

// Literals
// --------
LITERAL_REAL : [0-9]*'.'[0-9]+ EXPONENT? | [0-9]+'.' EXPONENT?;

// Others
// ------
IDENTIFIER : FRAGMENT_IDENTIFIER ;
WHITESPACE : FRAGMENT_WHITESPACE -> channel(HIDDEN) ;
LINE_TERMINATOR : FRAGMENT_LINE_TERMINATOR -> channel(HIDDEN) ;

// Fragments
// =========
fragment FRAGMENT_IDENTIFIER : IDENTIFIER_START IDENTIFIER_PART* ;
fragment FRAGMENT_WHITESPACE : [\t\u000B\u000C\u0020\u00A0] ;
fragment FRAGMENT_LINE_TERMINATOR : [\r\n\u2028\u2029] ;
fragment IDENTIFIER_START : [a-zA-Z_] ;
fragment IDENTIFIER_PART : [a-zA-Z0-9_] ;
fragment EXPONENT : [eE] [+-]? [0-9]+ ;
