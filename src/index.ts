import { generate, generateFromFile, execute, type API } from './lib/api-generator.js'
import { api } from './doc/api.js'


export {
  api,
  generate,
  generateFromFile,
  execute,
  type API
}
