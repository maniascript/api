import { AbstractParseTreeVisitor } from 'antlr4ng'
import { DocParserVisitor } from '../antlr/DocParserVisitor.js'
import {
  ClassDeclarationContext,
  EnumDeclarationContext,
  FunctionDeclarationContext,
  FunctionParameterContext,
  MemberDeclarationListContext,
  NamespaceDeclarationContext,
  TypeContext,
  VariableDeclarationContext
} from '../antlr/DocParser.js'

type Enum = string[]

type TypeCategory = 'class' | 'literal' | 'array' | 'enum'

interface Type {
  category: TypeCategory
  isValueConst?: boolean
  isPointer?: boolean
  isPointerConst?: boolean
  name?: string
  namespace?: string
  type?: Type
}

interface Variable {
  isConst?: boolean
  type: Type
}

interface FunctionDeclaration {
  type: Type
  parameters: FunctionParameter[]
}

interface FunctionParameter {
  type: Type
  name: string
}

interface ClassContent {
  parent?: string
  enums?: Record<string, Enum>
  variables?: Record<string, Variable>
  functions?: Record<string, FunctionDeclaration[]>
}

interface API {
  classNames: string[]
  namespaceNames: string[]
  classes: Record<string, ClassContent>
  namespaces: Record<string, ClassContent>
}

class DocVisitorError extends Error {}

function validateVisit<T>(value: T | null | undefined, name: string): T {
  if (value === null || value === undefined) {
    throw new DocVisitorError(`Failed to visit ${name}`)
  }
  return value
}

class EnumDeclarationVisitor extends AbstractParseTreeVisitor<Enum> implements DocParserVisitor<Enum> {
  defaultResult (): Enum {
    return []
  }

  visitEnumDeclaration (ctx: EnumDeclarationContext): Enum {
    const result = this.defaultResult()
    for (const value of ctx.enumValueList()._values) {
      if (value._name?.text !== undefined) {
        result.push(value._name.text)
      }
    }
    return result
  }
}

class TypeVisitor extends AbstractParseTreeVisitor<Type> implements DocParserVisitor<Type> {
  defaultResult (): Type {
    return {
      category: 'literal'
    }
  }

  visitType (ctx: TypeContext): Type {
    const result = this.defaultResult()

    if (ctx.typeLiteral() !== null) {
      result.category = 'literal'
      result.name = ctx.typeLiteral()?.getChild(0)?.getText()
    } else if (ctx.typeClass() !== null) {
      result.category = 'class'
      result.name = ctx.typeClass()?.getChild(0)?.getText()
    } else if (ctx.typeEnum() !== null) {
      result.category = 'enum'
      result.namespace = ctx.typeEnum()?.getChild(0)?.getText()
      result.name = ctx.typeEnum()?.getChild(2)?.getText()
    } else if (ctx.typeArray() !== null) {
      result.category = 'array'
      result.type = validateVisit(ctx.typeArray()?.getChild(2)?.accept(this), 'type')
    }

    result.isValueConst = (ctx._isValueConst !== undefined) ? true : undefined
    result.isPointer = (ctx._isPointer !== undefined) ? true : undefined
    result.isPointerConst = (ctx._isPointerConst !== undefined) ? true : undefined

    return result
  }
}

class FunctionParameterVisitor extends AbstractParseTreeVisitor<FunctionParameter> implements DocParserVisitor<FunctionParameter> {
  defaultResult (): FunctionParameter {
    return {
      type: {
        category: 'literal'
      },
      name: ''
    }
  }

  visitFunctionParameter (ctx: FunctionParameterContext): FunctionParameter {
    const result = this.defaultResult()
    result.type = validateVisit(ctx._parameterType?.accept(new TypeVisitor()), 'type')
    if (ctx._name?.text !== undefined) {
      result.name = ctx._name.text
    }
    return result
  }
}

class FunctionDeclarationVisitor extends AbstractParseTreeVisitor<FunctionDeclaration> implements DocParserVisitor<FunctionDeclaration> {
  defaultResult (): FunctionDeclaration {
    return {
      type: {
        category: 'literal'
      },
      parameters: []
    }
  }

  visitFunctionDeclaration (ctx: FunctionDeclarationContext): FunctionDeclaration {
    const result = this.defaultResult()
    result.type = validateVisit(ctx._functionType?.accept(new TypeVisitor()), 'type')
    const parameters = ctx.functionParameterList()?._parameters
    if (parameters !== undefined) {
      for (const parameter of parameters) {
        result.parameters.push(validateVisit(parameter.accept(new FunctionParameterVisitor()), 'function parameter'))
      }
    }
    return result
  }
}

class VariableDeclarationVisitor extends AbstractParseTreeVisitor<Variable> implements DocParserVisitor<Variable> {
  defaultResult (): Variable {
    return {
      type: {
        category: 'literal'
      }
    }
  }

  visitVariableDeclaration (ctx: VariableDeclarationContext): Variable {
    const result = this.defaultResult()
    result.isConst = (ctx._isConst !== undefined) ? true : undefined
    result.type = validateVisit(ctx._variableType?.accept(new TypeVisitor()), 'type')
    return result
  }
}

class MemberDeclarationListVisitor extends AbstractParseTreeVisitor<ClassContent> implements DocParserVisitor<ClassContent> {
  defaultResult (): ClassContent {
    return {}
  }

  visitMemberDeclarationList (ctx: MemberDeclarationListContext): ClassContent {
    const result = this.defaultResult()
    for (const child of ctx.children) {
      if (child instanceof VariableDeclarationContext) {
        if (child._name?.text !== undefined) {
          if (result.variables === undefined) {
            result.variables = {}
          }
          result.variables[child._name.text] = validateVisit(child.accept(new VariableDeclarationVisitor()), 'variable')
        }
      } else if (child instanceof FunctionDeclarationContext) {
        if (child._name?.text !== undefined) {
          if (result.functions === undefined) {
            result.functions = {}
          }
          const functionDeclaration = validateVisit(child.accept(new FunctionDeclarationVisitor()), 'function declaration')
          if (child._name.text in result.functions) {
            result.functions[child._name.text].push(functionDeclaration)
          } else {
            result.functions[child._name.text] = [functionDeclaration]
          }
        }
      } else if (child instanceof EnumDeclarationContext) {
        if (child._name?.text !== undefined) {
          if (result.enums === undefined) {
            result.enums = {}
          }
          result.enums[child._name.text] = validateVisit(child.accept(new EnumDeclarationVisitor()), 'enum')
        }
      }
    }
    return result
  }
}

class DocVisitor extends AbstractParseTreeVisitor<API> implements DocParserVisitor<API> {
  defaultResult (): API {
    return {
      classNames: [],
      namespaceNames: [],
      classes: {},
      namespaces: {}
    }
  }

  aggregateResult (aggregate: API, nextResult: API): API {
    return {
      classNames: [
        ...aggregate.classNames,
        ...nextResult.classNames
      ],
      namespaceNames: [
        ...aggregate.namespaceNames,
        ...nextResult.namespaceNames
      ],
      classes: {
        ...aggregate.classes,
        ...nextResult.classes
      },
      namespaces: {
        ...aggregate.namespaces,
        ...nextResult.namespaces
      }
    }
  }

  visitClassDeclaration (ctx: ClassDeclarationContext): API {
    const result = this.defaultResult()
    if (ctx._name?.text !== undefined) {
      result.classNames.push(ctx._name.text)
      if (ctx._content !== undefined) {
        result.classes[ctx._name.text] = validateVisit(ctx._content.accept(new MemberDeclarationListVisitor()), 'class content')
        if (ctx._parent?.text !== undefined) {
          result.classes[ctx._name.text].parent = ctx._parent.text
        }
      } else if (ctx._parent?.text !== undefined) {
        result.classes[ctx._name.text] = { parent: ctx._parent.text }
      } else {
        result.classes[ctx._name.text] = {}
      }
    }
    return result
  }

  visitNamespaceDeclaration (ctx: NamespaceDeclarationContext): API {
    const result = this.defaultResult()
    if (ctx._name?.text !== undefined) {
      result.namespaceNames.push(ctx._name.text)
      result.namespaces[ctx._name.text] = validateVisit(ctx._content?.accept(new MemberDeclarationListVisitor()), 'class content')
    }
    return result
  }
}

export {
  DocVisitor,
  validateVisit
}
export type {
  API
}
