import { resolve, dirname } from 'path'
import { CharStream, CommonTokenStream } from 'antlr4ng'
import { DocLexer } from '../antlr/DocLexer.js'
import { DocParser, ProgramContext } from '../antlr/DocParser.js'
import { DocVisitor, validateVisit } from './visitor.js'
import { readFile, writeFile } from 'fs/promises'

import type { API } from './visitor.js'

function parse (input = ''): ProgramContext {
  const inputStream = CharStream.fromString(input)
  const lexer = new DocLexer(inputStream)
  lexer.removeErrorListeners()

  const tokenStream = new CommonTokenStream(lexer)
  const parser = new DocParser(tokenStream)
  parser.removeErrorListeners()

  return parser.program()
}

function generate (input = ''): API {
  const tree = parse(input)
  return validateVisit(tree.accept(new DocVisitor()), 'documentation')
}

async function generateFromFile (path = ''): Promise<API> {
  const input = await readFile(path, 'utf8')
  return generate(input)
}

async function execute (path = ''): Promise<void> {
  const api = await generateFromFile(path)
  await Promise.all([
    writeFile(
      resolve(dirname(path), 'api.json'),
      JSON.stringify(api, null, 2)
    ),
    writeFile(
      resolve(dirname(path), 'api.ts'),
      `/* eslint-disable */\nimport type { API } from '../lib/visitor.js'\nexport const api: API = ${JSON.stringify(api, null, 2)}`
    )
  ])
}

export {
  generate,
  generateFromFile,
  execute,
  type API
}
