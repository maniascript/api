import { execute } from './lib/api-generator.js'

const [path] = process.argv.slice(2)

execute(path).catch((err: unknown) => {
  console.error(err)
})
