Changelog
=========

Unreleased
----------

- Nothing yet ⌛

Version 8.0.0
-------------

- [#31](https://gitlab.com/maniascript/api/-/issues/31) Added the parent class of each class in the `classes` object
- [#32](https://gitlab.com/maniascript/api/-/issues/32) Fixed missing empty classes in the `classes` object

Version 8.0.0
-------------

- [#30](https://gitlab.com/maniascript/api/-/issues/30) Exposed the `API` type
- [#29](https://gitlab.com/maniascript/api/-/issues/29) Updated the ManiaScript API to the latest Trackmania version

Version 7.0.0
-------------

- [#28](https://gitlab.com/maniascript/api/-/issues/28) Fixed types declaration exports for esm and cjs formats

Version 6.0.0
-------------

- [#26](https://gitlab.com/maniascript/api/-/issues/26) Use `antlr4ng` insteqd of `antlr4ts`
- [#27](https://gitlab.com/maniascript/api/-/issues/27) List namespace names

Version 5.0.1
-------------

- [#25](https://gitlab.com/maniascript/api/-/issues/25) Expose default exports

Version 5.0.0
-------------

- [#22](https://gitlab.com/maniascript/api/-/issues/22) Compatibility with the new documentation format
- [#23](https://gitlab.com/maniascript/api/-/issues/23) Use vitest instead of jest
- [#24](https://gitlab.com/maniascript/api/-/issues/24) Update dependencies

Version 4.0.0
-------------

- [#21](https://gitlab.com/maniascript/api/-/issues/21) Update the ManiaScript API to the latest Trackmania version

Version 3.1.0
-------------

- Do not bundle the module into a single file anymore
- [#20](https://gitlab.com/maniascript/api/-/issues/20) Update the ManiaScript API to the latest Trackmania version

Version 3.0.0
-------------

- [#19](https://gitlab.com/maniascript/api/-/issues/19) Full TypeScript rewrite

Version 2.3.0
-------------

- [#17](https://gitlab.com/maniascript/api/-/issues/17) Export the `generateFromFile` function
- [#18](https://gitlab.com/maniascript/api/-/issues/13) Update the default API exported by the module

Version 2.2.0
-------------

- [#16](https://gitlab.com/maniascript/api/-/issues/16) Add a function that takes a path to a Maniascript API doc file, parse the file and return the API.
- Update project dependencies.

Version 2.1.1
-------------

- [#4](https://gitlab.com/maniascript/api/-/issues/4) Rollback `CharStreams` hack for ANTLR

Version 2.1.0
-------------

- [#14](https://gitlab.com/maniascript/api/-/issues/14) Do not use the `fs/promises` module
- [#15](https://gitlab.com/maniascript/api/-/issues/15) Disable parallel tests in mocha

Version 2.0.1
-------------

- Fix wrong `LICENSE` file. Use the correct LGPL license now.

Version 2.0.0
-------------

- The module now export the api into two distinct objects: `classes` and `api`. `classes` is an array with the names of all classes available in the maniascript api. It's targeted to be used by the maniascript parser. `api` is the actual description of the full maniascript api.

Version 1.0.2
-------------

- Update project dependencies.

Version 1.0.0
-------------

- Initial release. 🎉
