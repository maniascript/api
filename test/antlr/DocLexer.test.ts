import { describe, test, expect } from 'vitest'
import { CharStream, Token } from 'antlr4ng'
import { DocLexer } from '../../src/antlr/DocLexer'

const getTokensDoc = (input: string): Token[] => {
  const chars = CharStream.fromString(input)
  const lexer = new DocLexer(chars)
  lexer.removeErrorListeners()
  return lexer.getAllTokens()
}

const validateToken = (token: Token, type: number, start: number, text: string): void => {
  expect(token.type).toBe(type)
  expect(token.start).toBe(start)
  expect(token.text).toBe(text)
}

describe('Lexer', () => {
  describe('find comments', () => {
    test('Single line comment', () => {
      const tokens = getTokensDoc('// Comment1')
      validateToken(tokens[0], DocLexer.SINGLE_LINE_COMMENT, 0, '// Comment1')
    })

    test('Multiple lines comment', () => {
      const tokens = getTokensDoc('/* Comment1 */' + '\n' + '/*\nComment2\n*/')
      validateToken(tokens[0], DocLexer.MULTI_LINES_COMMENT, 0, '/* Comment1 */')
      validateToken(tokens[2], DocLexer.MULTI_LINES_COMMENT, 15, '/*\nComment2\n*/')
    })
  })

  describe('find structures', () => {
    test('Single line structure', () => {
      let tokens = getTokensDoc('struct Void {};')
      validateToken(tokens[0], DocLexer.STRUCT_IGNORE, 0, 'struct Void {};')

      tokens = getTokensDoc(
        'struct Void {};' + '\n' +
        'struct Integer{};' + '\n' +
        'struct Real{};' + '\n' +
        'struct Boolean{};' + '\n' +
        'struct Text{};' + '\n' +
        'struct Vec2{Real X; Real Y;};' + '\n' +
        'struct Vec3{Real X; Real Y; Real Z;};' + '\n' +
        'struct Int2{Integer X; Integer Y;};' + '\n' +
        'struct Int3{Integer X; Integer Y; Integer Z;};' + '\n' +
        'struct Ident{};'
      )
      validateToken(tokens[0], DocLexer.STRUCT_IGNORE, 0, 'struct Void {};')
      validateToken(tokens[2], DocLexer.STRUCT_IGNORE, 16, 'struct Integer{};')
      validateToken(tokens[4], DocLexer.STRUCT_IGNORE, 34, 'struct Real{};')
      validateToken(tokens[6], DocLexer.STRUCT_IGNORE, 49, 'struct Boolean{};')
      validateToken(tokens[8], DocLexer.STRUCT_IGNORE, 67, 'struct Text{};')
      validateToken(tokens[10], DocLexer.STRUCT_IGNORE, 82, 'struct Vec2{Real X; Real Y;};')
      validateToken(tokens[12], DocLexer.STRUCT_IGNORE, 112, 'struct Vec3{Real X; Real Y; Real Z;};')
      validateToken(tokens[14], DocLexer.STRUCT_IGNORE, 150, 'struct Int2{Integer X; Integer Y;};')
      validateToken(tokens[16], DocLexer.STRUCT_IGNORE, 186, 'struct Int3{Integer X; Integer Y; Integer Z;};')
      validateToken(tokens[18], DocLexer.STRUCT_IGNORE, 233, 'struct Ident{};')
    })

    test('Multiple lines structure', () => {
      const tokens = getTokensDoc(
        'struct Array {' + '\n' +
          'ElemType operator[](Integer Index);' + '\n' +
          'ElemType operator[](Ident Id);' + '\n' +
          'ElemType operator[](CNod Object);' + '\n' +
          'Integer count;' + '\n' +
          'Array<ElemType> sort();' + '\n' +
          'Array<ElemType> sortreverse();' + '\n' +
          'Void add(ElemType Elem);' + '\n' +
          'Void addfirst(ElemType Elem);' + '\n' +
          'Boolean remove(ElemType Elem);' + '\n' +
          'Boolean exists(ElemType Elem);' + '\n' +
          'Integer keyof(ElemType Elem);' + '\n' +
          'Void clear();' + '\n' +
          'Boolean containsonly(Array<ElemType> Elem);' + '\n' +
          'Boolean containsoneof(Array<ElemType> Elem);' + '\n' +
          'Array<ElemType> slice(Integer Index);' + '\n' +
          'Array<ElemType> slice(Integer Index, Integer Count);' + '\n' +
        '};'
      )
      validateToken(tokens[0], DocLexer.STRUCT_IGNORE, 0, 'struct Array {' + '\n' +
        'ElemType operator[](Integer Index);' + '\n' +
        'ElemType operator[](Ident Id);' + '\n' +
        'ElemType operator[](CNod Object);' + '\n' +
        'Integer count;' + '\n' +
        'Array<ElemType> sort();' + '\n' +
        'Array<ElemType> sortreverse();' + '\n' +
        'Void add(ElemType Elem);' + '\n' +
        'Void addfirst(ElemType Elem);' + '\n' +
        'Boolean remove(ElemType Elem);' + '\n' +
        'Boolean exists(ElemType Elem);' + '\n' +
        'Integer keyof(ElemType Elem);' + '\n' +
        'Void clear();' + '\n' +
        'Boolean containsonly(Array<ElemType> Elem);' + '\n' +
        'Boolean containsoneof(Array<ElemType> Elem);' + '\n' +
        'Array<ElemType> slice(Integer Index);' + '\n' +
        'Array<ElemType> slice(Integer Index, Integer Count);' + '\n' +
      '};'
      )
    })
  })

  describe('find templates', () => {
    test('Single element template', () => {
      const tokens = getTokensDoc('template <typename ElemType>')
      validateToken(tokens[0], DocLexer.TEMPLATE_IGNORE, 0, 'template <typename ElemType>')
    })

    test('Multiple elements template', () => {
      const tokens = getTokensDoc('template <typename KeyType, typename ElemType>')
      validateToken(tokens[0], DocLexer.TEMPLATE_IGNORE, 0, 'template <typename KeyType, typename ElemType>')
    })
  })

  describe('find keywords', () => {
    test('class', () => {
      const tokens = getTokensDoc('class')
      validateToken(tokens[0], DocLexer.KEYWORD_CLASS, 0, 'class')
    })

    test('public', () => {
      const tokens = getTokensDoc('public')
      validateToken(tokens[0], DocLexer.KEYWORD_PUBLIC, 0, 'public')
    })

    test('enum', () => {
      const tokens = getTokensDoc('enum')
      validateToken(tokens[0], DocLexer.KEYWORD_ENUM, 0, 'enum')
    })

    test('const', () => {
      const tokens = getTokensDoc('const')
      validateToken(tokens[0], DocLexer.KEYWORD_CONST, 0, 'const')
    })

    test('Array', () => {
      const tokens = getTokensDoc('Array')
      validateToken(tokens[0], DocLexer.KEYWORD_ARRAY, 0, 'Array')
    })

    test('(reserved)', () => {
      const tokens = getTokensDoc('(reserved)')
      validateToken(tokens[0], DocLexer.KEYWORD_RESERVED, 0, '(reserved)')
    })

    test('*unused*', () => {
      const tokens = getTokensDoc('*unused*')
      validateToken(tokens[0], DocLexer.KEYWORD_UNUSED, 0, '*unused*')
    })

    test('namespace', () => {
      const tokens = getTokensDoc('namespace')
      validateToken(tokens[0], DocLexer.KEYWORD_NAMESPACE, 0, 'namespace')
    })
  })

  describe('find types', () => {
    test('Boolean', () => {
      const tokens = getTokensDoc('Boolean')
      validateToken(tokens[0], DocLexer.TYPE_BOOLEAN, 0, 'Boolean')
    })
    test('Ident', () => {
      const tokens = getTokensDoc('Ident')
      validateToken(tokens[0], DocLexer.TYPE_IDENT, 0, 'Ident')
    })
    test('Int2', () => {
      const tokens = getTokensDoc('Int2')
      validateToken(tokens[0], DocLexer.TYPE_INT2, 0, 'Int2')
    })
    test('Int3', () => {
      const tokens = getTokensDoc('Int3')
      validateToken(tokens[0], DocLexer.TYPE_INT3, 0, 'Int3')
    })
    test('Integer', () => {
      const tokens = getTokensDoc('Integer')
      validateToken(tokens[0], DocLexer.TYPE_INTEGER, 0, 'Integer')
    })
    test('Real', () => {
      const tokens = getTokensDoc('Real')
      validateToken(tokens[0], DocLexer.TYPE_REAL, 0, 'Real')
    })
    test('Text', () => {
      const tokens = getTokensDoc('Text')
      validateToken(tokens[0], DocLexer.TYPE_TEXT, 0, 'Text')
    })
    test('Vec2', () => {
      const tokens = getTokensDoc('Vec2')
      validateToken(tokens[0], DocLexer.TYPE_VEC2, 0, 'Vec2')
    })
    test('Vec3', () => {
      const tokens = getTokensDoc('Vec3')
      validateToken(tokens[0], DocLexer.TYPE_VEC3, 0, 'Vec3')
    })
    test('Void', () => {
      const tokens = getTokensDoc('Void')
      validateToken(tokens[0], DocLexer.TYPE_VOID, 0, 'Void')
    })
  })

  describe('find operators', () => {
    test(':', () => {
      const tokens = getTokensDoc(':')
      validateToken(tokens[0], DocLexer.OPERATOR_COLON, 0, ':')
    })
    test('::', () => {
      const tokens = getTokensDoc('::')
      validateToken(tokens[0], DocLexer.OPERATOR_DOUBLECOLON, 0, '::')
    })
    test(',', () => {
      const tokens = getTokensDoc(',')
      validateToken(tokens[0], DocLexer.OPERATOR_COMMA, 0, ',')
    })
    test('=', () => {
      const tokens = getTokensDoc('=')
      validateToken(tokens[0], DocLexer.OPERATOR_ASSIGN, 0, '=')
    })
    test(';', () => {
      const tokens = getTokensDoc(';')
      validateToken(tokens[0], DocLexer.OPERATOR_SEMICOLON, 0, ';')
    })
    test('{}', () => {
      const tokens = getTokensDoc('{}')
      validateToken(tokens[0], DocLexer.OPERATOR_OPEN_BRACE, 0, '{')
      validateToken(tokens[1], DocLexer.OPERATOR_CLOSE_BRACE, 1, '}')
    })
    test('<>', () => {
      const tokens = getTokensDoc('<>')
      validateToken(tokens[0], DocLexer.OPERATOR_OPEN_ANGLE, 0, '<')
      validateToken(tokens[1], DocLexer.OPERATOR_CLOSE_ANGLE, 1, '>')
    })
    test('()', () => {
      const tokens = getTokensDoc('()')
      validateToken(tokens[0], DocLexer.OPERATOR_OPEN_PAREN, 0, '(')
      validateToken(tokens[1], DocLexer.OPERATOR_CLOSE_PAREN, 1, ')')
    })
    test('*', () => {
      const tokens = getTokensDoc('*')
      validateToken(tokens[0], DocLexer.OPERATOR_POINTER, 0, '*')
    })
  })

  describe('find identifiers', () => {
    test('identifier', () => {
      const tokens = getTokensDoc('class CMlScript : public CNod {')
      validateToken(tokens[2], DocLexer.IDENTIFIER, 6, 'CMlScript')
      validateToken(tokens[8], DocLexer.IDENTIFIER, 25, 'CNod')
    })
  })

  describe('find literals', () => {
    test('Real', () => {
      const input = '1.0 1. .1 1.0e1 1.e1 .1e1 1.0e+1 1.0e-1 123.456 .123 123.'
      const tokens = getTokensDoc(input)
      const words = input.split(' ')
      let start = 0
      for (let key = 0; key < words.length; key++) {
        const word = words[key]
        validateToken(tokens[key * 2], DocLexer.LITERAL_REAL, start, word)
        start += word.length + ' '.length
      }
    })
  })
})
