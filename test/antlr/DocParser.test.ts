import { describe, test, expect } from 'vitest'
import { CommonTokenStream, CharStream } from 'antlr4ng'
import { DocLexer } from '../../src/antlr/DocLexer'
import { DocParser } from '../../src/antlr/DocParser'

const parseTreeStringDoc = (input: string): string => {
  const chars = CharStream.fromString(input)
  const lexer = new DocLexer(chars)
  lexer.removeErrorListeners()

  const tokens = new CommonTokenStream(lexer)
  const parser = new DocParser(tokens)
  parser.removeErrorListeners()

  const tree = parser.program()
  return tree.toStringTree(parser)
}

describe('Parser', () => {
  describe('ignore', () => {
    test('empty struct', () => {
      expect(parseTreeStringDoc(`
        struct Void {};
        struct Integer{};
        struct Real{};
        struct Boolean{};
        struct Text{};
        struct Vec2{Real X; Real Y;};
        struct Vec3{Real X; Real Y; Real Z;};
        struct Int2{Integer X; Integer Y;};
        struct Int3{Integer X; Integer Y; Integer Z;};
        struct Ident{};
      `)).toBe('(program <EOF>)')
    })

    test('filled struct', () => {
      expect(parseTreeStringDoc(`
        struct Array {
          ElemType operator[](Integer Index);
          ElemType operator[](Ident Id);
          ElemType operator[](CNod Object);
          Integer count;
          Array<ElemType> sort();
          Array<ElemType> sortreverse();
          Void add(ElemType Elem);
          Void addfirst(ElemType Elem);
          Boolean remove(ElemType Elem);
          Boolean exists(ElemType Elem);
          Integer keyof(ElemType Elem);
          Void clear();
          Boolean containsonly(Array<ElemType> Elem);
          Boolean containsoneof(Array<ElemType> Elem);
          Array<ElemType> slice(Integer Index);
          Array<ElemType> slice(Integer Index, Integer Count);
        };
      `)).toBe('(program <EOF>)')
    })

    test('template', () => {
      expect(parseTreeStringDoc(`
        template <typename ElemType>
      `)).toBe('(program <EOF>)')
    })
  })

  describe('Class declaration', () => {
    test('without inheritance', () => {
      expect(parseTreeStringDoc(`
        class CNod {
        public :
          Ident  const Id;
        };
      `)).toBe('(program (classDeclaration class CNod { public : (memberDeclarationList (variableDeclaration (type (typeLiteral Ident) const) Id ;)) } ;) <EOF>)')
    })

    test('with inheritance', () => {
      expect(parseTreeStringDoc(`
        class CNod : public CFake {
        public :
          Ident  const Id;
        };
      `)).toBe('(program (classDeclaration class CNod : public CFake { public : (memberDeclarationList (variableDeclaration (type (typeLiteral Ident) const) Id ;)) } ;) <EOF>)')
    })
  })

  describe('Namespace declaration', () => {
    test('namespace', () => {
      expect(parseTreeStringDoc(`
        namespace TextLib {
          Real ToReal(Text _Text);
        };
      `)).toBe('(program (namespaceDeclaration namespace TextLib { (memberDeclarationList (functionDeclaration (type (typeLiteral Real)) ToReal ( (functionParameterList (functionParameter (type (typeLiteral Text)) _Text)) ) ;)) } ;) <EOF>)')
    })
  })

  describe('Class/Namespace content', () => {
    test('variable declaration', () => {
      expect(parseTreeStringDoc(`
        class CMlScript : public CNod {
        public :
          Integer ArenaNow;
          Integer const Now;
          CMlPage AAA;
          CMlPage* BBB;
          CMlPage * CCC;
          CMlPage const DDD;
          CMlPage * const Page;
          CSmPlayer const * const InputPlayer;
        };
      `)).toBe('(program (classDeclaration class CMlScript : public CNod { public : (memberDeclarationList (variableDeclaration (type (typeLiteral Integer)) ArenaNow ;) (variableDeclaration (type (typeLiteral Integer) const) Now ;) (variableDeclaration (type (typeClass CMlPage)) AAA ;) (variableDeclaration (type (typeClass CMlPage) *) BBB ;) (variableDeclaration (type (typeClass CMlPage) *) CCC ;) (variableDeclaration (type (typeClass CMlPage) const) DDD ;) (variableDeclaration (type (typeClass CMlPage) * const) Page ;) (variableDeclaration (type (typeClass CSmPlayer) const * const) InputPlayer ;)) } ;) <EOF>)')
      expect(parseTreeStringDoc(`
        namespace MathLib {
          const Real Pi = 3.14159;
          const Real Tau = 6.28319;
          Integer Abs(Integer _Argument1);
        };
      `)).toBe('(program (namespaceDeclaration namespace MathLib { (memberDeclarationList (variableDeclaration const (type (typeLiteral Real)) Pi = 3.14159 ;) (variableDeclaration const (type (typeLiteral Real)) Tau = 6.28319 ;) (functionDeclaration (type (typeLiteral Integer)) Abs ( (functionParameterList (functionParameter (type (typeLiteral Integer)) _Argument1)) ) ;)) } ;) <EOF>)')
    })

    test('array', () => {
      expect(parseTreeStringDoc(`
        class CSmAction : public CAction {
        public :
          Array<CSmPlayer> Players;
          Array<CUser::ETagType> Tags_Type;
          Array<CMlScriptEvent* const > PendingEvents;
          Array<CSmPlayer const * const > Players;
          Void SendRulesEvent(Text Param1,Array<Text> Param2,CEntity Shooter,CEntity Victim);
        };
      `)).toBe('(program (classDeclaration class CSmAction : public CAction { public : (memberDeclarationList (variableDeclaration (type (typeArray Array < (type (typeClass CSmPlayer)) >)) Players ;) (variableDeclaration (type (typeArray Array < (type (typeEnum CUser :: ETagType)) >)) Tags_Type ;) (variableDeclaration (type (typeArray Array < (type (typeClass CMlScriptEvent) * const) >)) PendingEvents ;) (variableDeclaration (type (typeArray Array < (type (typeClass CSmPlayer) const * const) >)) Players ;) (functionDeclaration (type (typeLiteral Void)) SendRulesEvent ( (functionParameterList (functionParameter (type (typeLiteral Text)) Param1) , (functionParameter (type (typeArray Array < (type (typeLiteral Text)) >)) Param2) , (functionParameter (type (typeClass CEntity)) Shooter) , (functionParameter (type (typeClass CEntity)) Victim)) ) ;)) } ;) <EOF>)')
    })

    test('enum', () => {
      expect(parseTreeStringDoc(`
        class CSmPlayer : public CPlayer {
        public :
          enum ESpawnStatus {
            NotSpawned,
            (reserved),
            Spawning,
            Spawned,
            XXX Null,
            *unused*,
          };
          CSmPlayer::ESpawnStatus SpawnStatusA;
          CSmPlayer::ESpawnStatus const  SpawnStatusB;
        };
      `)).toBe('(program (classDeclaration class CSmPlayer : public CPlayer { public : (memberDeclarationList (enumDeclaration enum ESpawnStatus { (enumValueList (enumValue NotSpawned) , (enumValue (reserved)) , (enumValue Spawning) , (enumValue Spawned) , (enumValue XXX Null) , (enumValue *unused*) ,) } ;) (variableDeclaration (type (typeEnum CSmPlayer :: ESpawnStatus)) SpawnStatusA ;) (variableDeclaration (type (typeEnum CSmPlayer :: ESpawnStatus) const) SpawnStatusB ;)) } ;) <EOF>)')
    })

    test('function declaration', () => {
      expect(parseTreeStringDoc(`
        class CMlScript : public CNod {
        public :
          Boolean Cooldown_IsReady();
          Void Dbg_SetProcessed(CMlScriptEvent Event);
          Void EnableMenuNavigation(Boolean EnableInputs,Boolean WithAutoFocus,CMlControl AutoBackControl,Integer InputPriority);
          Void OpenLink(Text Url,CMlScript::LinkType LinkType);
        };
      `)).toBe('(program (classDeclaration class CMlScript : public CNod { public : (memberDeclarationList (functionDeclaration (type (typeLiteral Boolean)) Cooldown_IsReady ( ) ;) (functionDeclaration (type (typeLiteral Void)) Dbg_SetProcessed ( (functionParameterList (functionParameter (type (typeClass CMlScriptEvent)) Event)) ) ;) (functionDeclaration (type (typeLiteral Void)) EnableMenuNavigation ( (functionParameterList (functionParameter (type (typeLiteral Boolean)) EnableInputs) , (functionParameter (type (typeLiteral Boolean)) WithAutoFocus) , (functionParameter (type (typeClass CMlControl)) AutoBackControl) , (functionParameter (type (typeLiteral Integer)) InputPriority)) ) ;) (functionDeclaration (type (typeLiteral Void)) OpenLink ( (functionParameterList (functionParameter (type (typeLiteral Text)) Url) , (functionParameter (type (typeEnum CMlScript :: LinkType)) LinkType)) ) ;)) } ;) <EOF>)')
    })

    test('function declaration with parameter named Text', () => {
      expect(parseTreeStringDoc(`
        class CUIConfig : public CNod {
        public :
          Void SendChat(Text Text);
        };
      `)).toBe('(program (classDeclaration class CUIConfig : public CNod { public : (memberDeclarationList (functionDeclaration (type (typeLiteral Void)) SendChat ( (functionParameterList (functionParameter (type (typeLiteral Text)) Text)) ) ;)) } ;) <EOF>)')
    })
  })
})
