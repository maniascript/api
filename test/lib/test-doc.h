
class CMlScript : public CNod {
public :
  enum LinkType {
    ExternalBrowser,
    ManialinkBrowser,
    Goto,
    ExternalFromId,
    ManialinkFromId,
    GotoFromId,
  };
  Boolean PageAlwaysUpdateScript;
  Integer  const Now;
  CMlPage * const  Page;
  Array<CMlScriptEvent* const > PendingEvents;
  Void Dbg_SetProcessed(CMlScriptEvent Event);
  Boolean IsKeyPressed(Integer KeyCode);
  Void EnableMenuNavigation(Boolean EnableInputs,Boolean WithAutoFocus,CMlControl AutoBackControl,Integer InputPriority);
  Void EnableMenuNavigation(Boolean EnableInputs,Boolean WithAutoFocus,Boolean WithManualScroll,CMlControl AutoBackControl,Integer InputPriority);
  Void OpenLink(Text Url,CMlScript::LinkType LinkType);
  Void SendCustomEvent(Text Type,Array<Text> Data);
};
class CSmArenaInterfaceManialinkScriptHandler_ReadOnly : public CMlScriptIngame_ReadOnly {
public :
	Integer ArenaNow;
	CSmPlayer const  * const  InputPlayer;
	Array<CSmPlayer const * const > Players;
};
class CManiaApp : public CNod {
public :
  enum ELinkType {
    ExternalBrowser,
    ManialinkBrowser,
  };
  Text  const ManiaAppUrl;
  Text  const ManiaAppBaseUrl;
};
class CSystemPlatform : public CNod {
public :
	enum ESystemPlatform {
		None,
		Steam,
		UPlay,
		PS4,
		XBoxOne,
		PS5,
		XBoxSeries,
		Stadia,
		Luna,
	};
	CSystemPlatform::ESystemPlatform const  Platform;
};
class CNod {
public :
	Ident  const Id;
};
class CDirectLink {
public :
};
namespace MathLib {
  const Real Pi = 3.14159;
  const Real Tau = 6.28319;
  Integer Abs(Integer _Argument1);
  Real Abs(Real _Argument1);
  Real ToReal(Integer _Argument1);
  Real DegToRad(Real _Degree);
  Real RadToDeg(Real _Radian);
  Real Sin(Real _Argument1);
  Real Cos(Real _Argument1);
  Real Tan(Real _Argument1);
  Real Atan2(Real _Argument1, Real _Argument2);
  Real Exp(Real _Argument1);
  Real Rand(Real _Argument1, Real _Argument2);
  Real Rand(Real _Argument1, Real _Argument2, Integer _Argument3);
  Integer Rand(Integer _Argument1, Integer _Argument2);
  Integer Rand(Integer _Argument1, Integer _Argument2, Integer _Argument3);
  Real NearestReal(Integer _Argument1);
  Integer NearestInteger(Real _Argument1);
  Integer FloorInteger(Real _Argument1);
  Integer TruncInteger(Real _Argument1);
  Integer CeilingInteger(Real _Argument1);
  Real Distance(Real _Argument1, Real _Argument2);
  Real Distance(Vec2 _Argument1, Vec2 _Argument2);
  Real Distance(Vec3 _Argument1, Vec3 _Argument2);
  Real Length(Vec2 _Argument1);
  Real Length(Vec3 _Argument1);
  Real Norm0(Vec2 _Argument1);
  Real Norm1(Vec2 _Argument1);
  Real Norm0(Vec3 _Argument1);
  Real Norm1(Vec3 _Argument1);
  Integer Norm0(Int2 _Argument1);
  Integer Norm1(Int2 _Argument1);
  Integer Norm0(Int3 _Argument1);
  Integer Norm1(Int3 _Argument1);
  Real DotProduct(Vec3 _Argument1, Vec3 _Argument2);
  Vec3 CrossProduct(Vec3 _Argument1, Vec3 _Argument2);
  Real DotProduct(Vec2 _Argument1, Vec2 _Argument2);
  Integer DotProduct(Int3 _Argument1, Int3 _Argument2);
  Int3 CrossProduct(Int3 _Argument1, Int3 _Argument2);
  Integer DotProduct(Int2 _Argument1, Int2 _Argument2);
  Real Angle(Vec3 _Argument1, Vec3 _Argument2);
  Real OrientedAngle(Vec3 _Argument1, Vec3 _Argument2);
  Real Angle(Real _Radian1, Real _Radian2);
  Real Angle(Vec2 _Argument1, Vec2 _Argument2);
  Real OrientedAngle(Vec2 _Argument1, Vec2 _Argument2);
  Real PI();
  Real Asin(Real _Argument1);
  Real Acos(Real _Argument1);
  Real Pow(Real _Argument1, Real _Argument2);
  Real Ln(Real _Argument1);
  Real Sqrt(Real _Argument1);
  Integer Max(Integer _A, Integer _B);
  Integer Min(Integer _A, Integer _B);
  Integer Clamp(Integer _X, Integer _Min, Integer _Max);
  Real Max(Real _A, Real _B);
  Real Min(Real _A, Real _B);
  Real Clamp(Real _X, Real _Min, Real _Max);
  Real Mod(Real _X, Real _Min, Real _Max);
  Vec2 Max(Vec2 _A, Vec2 _B);
  Vec2 Min(Vec2 _A, Vec2 _B);
  Vec2 Clamp(Vec2 _X, Vec2 _Min, Vec2 _Max);
  Vec3 Max(Vec3 _A, Vec3 _B);
  Vec3 Min(Vec3 _A, Vec3 _B);
  Vec3 Clamp(Vec3 _X, Vec3 _Min, Vec3 _Max);
  Int2 Max(Int2 _A, Int2 _B);
  Int2 Min(Int2 _A, Int2 _B);
  Int2 Clamp(Int2 _X, Int2 _Min, Int2 _Max);
  Int3 Max(Int3 _A, Int3 _B);
  Int3 Min(Int3 _A, Int3 _B);
  Int3 Clamp(Int3 _X, Int3 _Min, Int3 _Max);
};
namespace TextLib {
  Real ToReal(Text _Text);
  Integer ToInteger(Text _Text);
  Vec3 ToColor(Text _Text);
  Text SubString(Text _Text, Integer _Start, Integer _Length);
  Text SubText(Text _Text, Integer _Start, Integer _Length);
  Integer Length(Text _Text);
  Text ToText(Integer _Integer);
  Text ToText(Real _Real);
  Text ToText(Boolean _Boolean);
  Text ToText(Int3 _Int3);
  Text ToText(Vec3 _Vec3);
  Text TimeToText(Integer _Time);
  Text TimeToText(Integer _Time, Boolean _IncludeCentiSeconds);
  Text TimeToText(Integer _Time, Boolean _IncludeCentiSeconds, Boolean _IncludeMilliSeconds);
  Text ColorToText(Vec3 _Color);
  Text FormatInteger(Integer _Argument1, Integer _Argument2);
  Text FormatRank(Integer _Rank, Boolean _ShortFormat);
  Text FormatReal(Real _Value, Integer _FPartLength, Boolean _HideZeroes, Boolean _HideDot);
  Text ToUpperCase(Text _TextToChange);
  Text ToLowerCase(Text _TextToChange);
  Text CloseStyleTags(Text _String);
  Boolean CompareWithoutFormat(Text _Text1, Text _Text2, Boolean _IsCaseSensitive);
  Boolean Find(Text _TextToFind, Text _TextToSearchIn, Boolean _IsFormatSensitive, Boolean _IsCaseSensitive);
  Boolean EndsWith(Text _TextToFind, Text _TextToSearchIn);
  Boolean EndsWith(Text _TextToFind, Text _TextToSearchIn, Boolean _IsFormatSensitive, Boolean _IsCaseSensitive);
  Boolean StartsWith(Text _TextToFind, Text _TextToSearchIn);
  Boolean StartsWith(Text _TextToFind, Text _TextToSearchIn, Boolean _IsFormatSensitive, Boolean _IsCaseSensitive);
  Text Compose(Text _Argument1);
  Text Compose(Text _Argument1, Text _Argument2);
  Text Compose(Text _Argument1, Text _Argument2, Text _Argument3);
  Text Compose(Text _Argument1, Text _Argument2, Text _Argument3, Text _Argument4);
  Text Compose(Text _Argument1, Text _Argument2, Text _Argument3, Text _Argument4, Text _Argument5);
  Text Compose(Text _Argument1, Text _Argument2, Text _Argument3, Text _Argument4, Text _Argument5, Text _Argument6);
  Text MLEncode(Text _Argument1);
  Text URLEncode(Text _Argument1);
  Text StripFormatting(Text _Argument1);
  Array<Text> Split(Text _Separators, Text _Text);
  Array<Text> Split(Text _Separators, Text _Text, Boolean _MergeSeparators);
  Text Join(Text _Separator, Array<Text> _Texts);
  Text Trim(Text _Argument1);
  Text ReplaceChars(Text _Argument1, Text _Argument2, Text _Argument3);
  Text Replace(Text _Text, Text _ToReplace, Text _Replacement);
  Array<Text> RegexFind(Text _Pattern, Text _Text, Text _Flags);
  Array<Text> RegexMatch(Text _Pattern, Text _Text, Text _Flags);
  Text RegexReplace(Text _Pattern, Text _Text, Text _Flags, Text _Replacement);
  Text GetTranslatedText(Text _Text);
};
