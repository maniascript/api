import { describe, test, expect, beforeAll, afterAll } from 'vitest'
import { unlink, stat } from 'node:fs/promises'
import { readFileSync } from 'node:fs'
import { generate, generateFromFile, execute } from '../../src/lib/api-generator'
import type { API } from '../../src/lib/visitor'

let api: API
const docH: string = readFileSync('./test/lib/test-doc.h', 'utf8')

describe('API generator', () => {
  describe('generate', () => {
    beforeAll(() => {
      api = generate(docH)
    })

    test('find class names', () => {
      expect('classNames' in api).toBe(true)
      expect(Array.isArray(api.classNames)).toBe(true)
      expect(api.classNames.length).toBe(6)
    })

    test('find namespace names', () => {
      expect('namespaceNames' in api).toBe(true)
      expect(Array.isArray(api.namespaceNames)).toBe(true)
      expect(api.namespaceNames.length).toBe(2)
      expect(api.namespaceNames).toContain('MathLib')
      expect(api.namespaceNames).toContain('TextLib')
    })

    test('find classes', () => {
      expect('classes' in api).toBe(true)
      expect(Object.getOwnPropertyNames(api.classes).length).toBe(6)
      expect(Object.prototype.hasOwnProperty.call(api.classes, 'CMlScript')).toBe(true)
      expect(Object.prototype.hasOwnProperty.call(api.classes, 'CSmArenaInterfaceManialinkScriptHandler_ReadOnly')).toBe(true)
      expect(Object.prototype.hasOwnProperty.call(api.classes, 'CManiaApp')).toBe(true)
      expect(Object.prototype.hasOwnProperty.call(api.classes, 'CSystemPlatform')).toBe(true)
      expect(Object.prototype.hasOwnProperty.call(api.classes, 'CNod')).toBe(true)
      expect(Object.prototype.hasOwnProperty.call(api.classes, 'CDirectLink')).toBe(true)
    })

    test('find namespaces', () => {
      expect('namespaces' in api).toBe(true)
      expect(Object.getOwnPropertyNames(api.namespaces).length).toBe(2)
      expect(Object.prototype.hasOwnProperty.call(api.namespaces, 'TextLib')).toBe(true)
      expect(Object.prototype.hasOwnProperty.call(api.namespaces, 'MathLib')).toBe(true)
    })

    describe('find class  parent', () => {
      test('has "parent" property', () => {
        expect('parent' in api.classes['CMlScript']).toBe(true)
        expect('parent' in api.classes['CNod']).toBe(false)
      })

      test('has correct parent', () => {
        expect(api.classes['CMlScript'].parent).toBe('CNod')
      })
    })

    describe('find functions', () => {
      test('has "functions" property', () => {
        const targets = [api.classes['CMlScript'], api.namespaces['TextLib']]
        for (const target of targets) {
          expect('functions' in target).toBe(true)
        }
      })
      test('list all functions', () => {
        expect(Object.getOwnPropertyNames(api.classes['CMlScript'].functions).length).toBe(5)
        const functionsName = ['Dbg_SetProcessed', 'IsKeyPressed', 'EnableMenuNavigation', 'OpenLink']
        for (const functionName of functionsName) {
          expect(Object.prototype.hasOwnProperty.call(api.classes['CMlScript'].functions, functionName)).toBe(true)
        }
      })
      test('list all functions variants', () => {
        expect(api.classes['CMlScript'].functions?.['Dbg_SetProcessed'].length).toBe(1)
        expect(api.classes['CMlScript'].functions?.['IsKeyPressed'].length).toBe(1)
        expect(api.classes['CMlScript'].functions?.['EnableMenuNavigation'].length).toBe(2)
        expect(api.classes['CMlScript'].functions?.['OpenLink'].length).toBe(1)
      })
      test('has function properties', () => {
        expect(Object.getOwnPropertyNames(api.classes['CMlScript'].functions?.['EnableMenuNavigation'][0]).length).toBe(2)
        expect(Object.prototype.hasOwnProperty.call(api.classes['CMlScript'].functions?.['EnableMenuNavigation'][0], 'type')).toBe(true)
        expect(Object.prototype.hasOwnProperty.call(api.classes['CMlScript'].functions?.['EnableMenuNavigation'][0], 'parameters')).toBe(true)
      })
      test('get function type', () => {
        expect(api.classes['CMlScript'].functions?.['EnableMenuNavigation'][0].type).toEqual({
          category: 'literal',
          name: 'Void'
        })
      })
      test('get function parameters', () => {
        expect(api.classes['CMlScript'].functions?.['EnableMenuNavigation'][0].parameters.length).toBe(4)
        expect(api.classes['CMlScript'].functions?.['EnableMenuNavigation'][0].parameters[0]).toEqual({
          type: {
            category: 'literal',
            name: 'Boolean'
          },
          name: 'EnableInputs'
        })
        expect(api.classes['CMlScript'].functions?.['EnableMenuNavigation'][0].parameters[1]).toEqual({
          type: {
            category: 'literal',
            name: 'Boolean'
          },
          name: 'WithAutoFocus'
        })
        expect(api.classes['CMlScript'].functions?.['EnableMenuNavigation'][0].parameters[2]).toEqual({
          type: {
            category: 'class',
            name: 'CMlControl'
          },
          name: 'AutoBackControl'
        })
        expect(api.classes['CMlScript'].functions?.['EnableMenuNavigation'][0].parameters[3]).toEqual({
          type: {
            category: 'literal',
            name: 'Integer'
          },
          name: 'InputPriority'
        })
      })
    })

    describe('find variables', () => {
      test('has "variables" property', () => {
        expect('variables' in api.classes['CMlScript']).toBe(true)
        expect('variables' in api.namespaces['TextLib']).toBe(false)
        expect('variables' in api.namespaces['MathLib']).toBe(true)
      })
      test('list all variables', () => {
        expect(Object.getOwnPropertyNames(api.classes['CMlScript'].variables).length).toBe(4)
        const variablesName = ['Page', 'PendingEvents', 'PageAlwaysUpdateScript', 'Now']
        for (const variableName of variablesName) {
          expect(Object.prototype.hasOwnProperty.call(api.classes['CMlScript'].variables, variableName)).toBe(true)
        }
      })
      test('variable has "isConst" property', () => {
        expect(api.classes['CMlScript'].variables?.['PendingEvents'].isConst).toBe(undefined)
        expect(api.classes['CMlScript'].variables?.['PageAlwaysUpdateScript'].isConst).toBe(undefined)
        expect(Object.prototype.hasOwnProperty.call(api.namespaces['MathLib'].variables?.['Pi'], 'isConst')).toBe(true)
        expect(api.namespaces['MathLib'].variables?.['Pi'].isConst).toBe(true)
      })
      test('variable has "type" property', () => {
        expect(Object.prototype.hasOwnProperty.call(api.classes['CMlScript'].variables?.['PendingEvents'], 'type')).toBe(true)
        expect(api.classes['CMlScript'].variables?.['PendingEvents'].type).toEqual({
          category: 'array',
          type: {
            category: 'class',
            name: 'CMlScriptEvent',
            isValueConst: true,
            isPointer: true
          }
        })
      })
    })

    describe('find correct types', () => {
      test('basic', () => {
        expect(api.classes['CMlScript'].variables?.['PageAlwaysUpdateScript'].type).toEqual({
          category: 'literal',
          name: 'Boolean'
        })
      })
      test('isValueConst', () => {
        expect(api.classes['CMlScript'].variables?.['Now'].type).toEqual({
          category: 'literal',
          isValueConst: true,
          name: 'Integer'
        })
      })
      test('isPointer', () => {
        expect(api.classes['CMlScript'].variables?.['Page'].type).toEqual({
          category: 'class',
          isValueConst: true,
          isPointer: true,
          name: 'CMlPage'
        })
      })
      test('isPointerConst', () => {
        expect(api.classes['CSmArenaInterfaceManialinkScriptHandler_ReadOnly'].variables?.['InputPlayer'].type).toEqual({
          category: 'class',
          isValueConst: true,
          isPointer: true,
          isPointerConst: true,
          name: 'CSmPlayer'
        })
      })
    })

    describe('find enums', () => {
      test('has enums properties', () => {
        expect('enums' in api.classes['CMlScript']).toBe(true)
        expect('enums' in api.namespaces['TextLib']).toBe(false)
        expect('enums' in api.namespaces['MathLib']).toBe(false)
      })
      test('list all enums', () => {
        expect(Object.getOwnPropertyNames(api.classes['CMlScript'].enums).length).toBe(1)
        expect(Object.prototype.hasOwnProperty.call(api.classes['CMlScript'].enums, 'LinkType')).toBe(true)
      })
      test('has enum values', () => {
        expect(api.classes['CMlScript'].enums?.['LinkType'].length).toBe(6)
        expect(api.classes['CMlScript'].enums?.['LinkType']).toEqual([
          'ExternalBrowser',
          'ManialinkBrowser',
          'Goto',
          'ExternalFromId',
          'ManialinkFromId',
          'GotoFromId'
        ])
      })
    })
  })

  describe('generateFromFile', () => {
    test('read documentation and output the maniascript api', async () => {
      const api = await generateFromFile('./test/lib/test-doc.h')
      expect('classNames' in api).toBe(true)
      expect('namespaceNames' in api).toBe(true)
      expect('classes' in api).toBe(true)
      expect('namespaces' in api).toBe(true)
    })
  })

  describe('execute', () => {
    const docPath = './test/lib/test-doc.h'
    const apiPath = './test/lib/api.json'
    const apiTsPath = './test/lib/api.ts'

    afterAll(async () => {
      await unlink(apiPath)
      await unlink(apiTsPath)
    })

    test('read documentation and output the maniascript api into a file', async () => {
      await execute(docPath)
      const stats = await stat(apiPath)
      expect(stats.isFile()).toBe(true)
    })
  })
})
